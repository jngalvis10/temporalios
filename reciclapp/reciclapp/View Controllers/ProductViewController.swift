//
//  ProductViewController.swift
//  reciclapp
//
//  Created by Juan Nicolas Galvis Ortiz on 11/7/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase


class ProductViewController: UIViewController, UIPickerViewDelegate,  UIPickerViewDataSource {
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
         return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    var pickerData: [String] = [String] ()
    var ref: DatabaseReference!
    var tipoProducto: String = ""
    
    @IBOutlet weak var nameTextField: UITextField!

    @IBOutlet weak var myMapRoundedButton: UIButton!
    
    
    
    @IBOutlet weak var tipo: UIPickerView!
    
   override func viewDidLoad() {
         tipoProducto = "Papel"
         self.hideKeyboardWhenTappedAround()
          let timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(fire), userInfo: nil, repeats: true)
          super.viewDidLoad()
            // Do any additional setup after loading the view, typically from a nib.
         self.myMapRoundedButton.layer.cornerRadius = 18
         
         self.tipo.delegate = self
         self.tipo.dataSource = self
            
            pickerData = ["Vidrio", "Metal", "Papel", "Plástico", "Cartón", "Otro"]
        }
    
    @IBAction func backtapped(_ sender: Any) {
        backto()
    }
    @IBAction func tapped(_ sender: Any) {
        let code = UserDefaults.standard.string(forKey: "codigo")!
        print (code)
            
         self.ref = Database.database().reference()
    
        print ("paso 2")
        let postObject = ["Descripcion": "Lave el producto con abundante agua y deposite en la caneca blanca de reciclaje", "nombre": nameTextField.text ?? "","tipoproducto": tipoProducto ?? "otro", "extra": "",  "cantidadreciclada": 0 ,  "codigo": code ] as [String : Any]
        self.ref.child("productos").child(code).setValue(postObject)
        backto()
        print ("we did it")
    }
    
      
    /* @IBAction func addTapped(_ sender: Any) {
         let code = UserDefaults.standard.string(forKey: "codigo")!
            
         self.ref = Database.database().reference()
        self.ref.child("productos").setValue(code)
        let postObject = ["Descripcion": "Lave el producto con abundante agua y deposite en la caneca blanca de reciclaje", "nombre": nameTextField.text ?? "","tipoproducto": tipoProducto ?? "otro", "extra": "",  "cantidadreciclada": 0 ,  "codigo": code ] as [String : Any]
        self.ref.child("productos").child(code).setValue(postObject)
                                                           
     }*/
     
        // Number of columns of data
      
        
        // The data to return fopr the row and component (column) that's being passed in
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         tipoProducto = pickerData[row]
            return pickerData[row]
        }
         @objc func fire()
           {
               if Connection.isConnected(vc: self) == false{
                   let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
                   
                   alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                   self.present(alert, animated: true)
               }
           }
     func pickerView(pickerView: UIPickerView, didSelectRow row: Int) {
         // This method is triggered whenever the user makes a change to the picker selection.
         // The parameter named row and component represents what was selected.
     
    
     }
    func backto() {
       
        if Connection.isConnected(vc: self) == true {
               print ("Rumbo a home")
            let homeViewController = storyboard?.instantiateViewController (withIdentifier: "tabbar") as! UITabBarController
                      
               
                
                    view.window?.rootViewController = homeViewController
                    view.window?.makeKeyAndVisible()
                print ("Lo logramos")
           }
          
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
