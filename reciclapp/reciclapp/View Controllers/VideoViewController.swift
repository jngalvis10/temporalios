//
//  VideoViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 12/3/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit

class VideoViewController: UIViewController {


    @IBOutlet weak var webPlayer: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.loadVideo()
        let myURL : NSURL = NSURL(string: "https://www.youtube.com/watch?v=_Ic_oz9FN44")!
        //Note: use the "embed" address instead of the "watch" address.
        let myURLRequest : NSURLRequest = NSURLRequest(url: myURL as URL)
        self.webPlayer.loadRequest(myURLRequest as URLRequest)
    }
    
    func loadVideo(){
        //playerView.load(withVideoId: "SaTgeUIyi0w")
    }
}
