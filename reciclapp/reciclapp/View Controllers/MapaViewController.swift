//
//  MapaViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/6/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import FirebaseDatabase
import GoogleMaps
import GooglePlaces

class MapaViewController: UIViewController{
//AIzaSyATJUcmWfi1oX3muNQUFQGR2HUNG-nYI2Y
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0

    // An array to hold the list of recycling places
    var likelyPlaces: [GMSPlace] = []

    // The currently selected place.
    var selectedPlace: GMSPlace?

    // A default location to use when location permission is not granted.
    //let defaultLocation = CLLocation(latitude: -33.869405, longitude: 151.199)
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Initialize the location manager.
//        locationManager = CLLocationManager()
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.requestAlwaysAuthorization()
//        locationManager.distanceFilter = 50
//        locationManager.startUpdatingLocation()
//        locationManager.delegate = self
//
//        placesClient = GMSPlacesClient.shared()
//    }
    
    override func loadView() {
         let timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(fire), userInfo: nil, repeats: true)
        // Create a GMSCameraPosition that tells the map to display the
           // coordinate -33.86,151.20 at zoom level 6.
           let camera = GMSCameraPosition.camera(withLatitude: 4.603107, longitude: -74.065212, zoom: 12.0)
           let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
           view = mapView
        
        if Connection.isConnected(vc: self) == false{
            fire()
        }
           // Creates a marker in the center of the map.
           let marker = GMSMarker()
           marker.position = CLLocationCoordinate2D(latitude: 4.603107, longitude: -74.065212)
           marker.title = "Universidad de los Andes"
           marker.snippet = "Punto de reciclaje de Cartón"
        
        marker.position = CLLocationCoordinate2D(latitude: 4.676107, longitude: -74.0714212)
        marker.title = "Centro de reciclaje chapinero"
        marker.snippet = "Punto de reciclaje multifacético"
        marker.position = CLLocationCoordinate2D(latitude: 4.702204, longitude: -74.0415)
        marker.title = "Centro de reciclaje unicentro"
        marker.snippet = "Punto de reciclaje de papel"
        marker.position = CLLocationCoordinate2D(latitude: 4.5986331, longitude: -74.0712374)
        marker.title = "Centro de reciclaje Plaza de Bolivar"
        marker.snippet = "Punto de reciclaje multifacético"
           marker.map = mapView
          // Enables asking for the location
           mapView.isMyLocationEnabled = true
           mapView.settings.myLocationButton = true
          //Create the markers for the centers
//           centersNear()
    }
    
    @objc func fire(){
        if Connection.isConnected(vc: self) == false{
            let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
               
             alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
             self.present(alert, animated: true)
        }
    }
    
    func centersNear(){
        
        
        
        
        
//        let ref = Database.database().reference()
//        ref.child("centros").observeSingleEvent(of:.value){ (snapshot) in
//            if snapshot.exists(){
//                if let location = snapshot.value as? [String: Any]{
//                    for eachLocation in location {
//                        print ("Location: \(eachLocation)")
//                        if let locationCoordinate = eachLocation.value as? [String : Any]{
//                            if let centerlatitude = locationCoordinate ["latitud"] as? Double{
//                                if let centerLongitud = locationCoordinate ["longitud"] as? Double {
//                                    print(centerlatitude)
//                                    print(centerLongitud)
//                                }
//                            }
//                        }
//                    }
//
//                }
//            }
//
//        }
        
        
    }
 }

//}
