//
//  ProfileViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/7/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import Firebase

class ProfileViewController: UIViewController {

    @IBOutlet weak var userLabel: UILabel!
    
    @IBOutlet weak var levelLabel: UILabel!
    
    @IBOutlet weak var percentageLabel: UILabel!
    
    @IBOutlet weak var cerrarbtn: UIButton!
    
    var refDB = DatabaseReference.init()
    
    override func viewDidLoad() {
         self.refDB = Database.database().reference()
        let name = UserDefaults.standard.string(forKey: "email") ?? "Usuario"
        var user = UserDefaults.standard.string(forKey: "user") ?? "angel"
        
        Connection.isConnected(vc: self)
            
            refDB.child("usuariosios/"+user+"/puntosAcumulados").observeSingleEvent(of: .value) { (snapshot) in
                  let points = snapshot.value as? Int
                let a = snapshot.value as? String
                print (a)
               
            
                 print (points)
                            if points != nil {
                               
                                self.userLabel.text = name
                                let nivel = 1 + (points!/50)
                                self.levelLabel.text = "Nivel " + "\(1 + (points!/50))"
                                self.percentageLabel.text = "\((100*(points!/50))/nivel)"+" %"
                                           super.viewDidLoad()
                    }
                            else{
                                let points = 0
                                self.userLabel.text = name
                                self.levelLabel.text = "Nivel " + "\(1 + (points/50))"
                                self.percentageLabel.text = "\(100*(points/50))"+" %"
                                super.viewDidLoad()
                                
                }
                        
                }
         
         userLabel.text = name
      
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    

    @IBAction func CerrarTapped(_ sender: Any) {
        let homeViewController = storyboard?.instantiateViewController (withIdentifier: "LoginVC") as! UIViewController
        
         
             view.window?.rootViewController = homeViewController
             view.window?.makeKeyAndVisible()
         print ("lo logramos")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
