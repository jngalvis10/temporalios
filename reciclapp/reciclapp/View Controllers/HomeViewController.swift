//
//  HomeViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/13/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import Firebase

class HomeViewController: UIViewController {
    //public var 

    @IBOutlet weak var PointsButton: UIButton!
     var refDB = DatabaseReference.init()
    
    override func viewDidLoad() {
        self.refDB = Database.database().reference()
        
              
               var user = UserDefaults.standard.string(forKey: "user") ?? "angel"

                   refDB.child("usuariosios/"+user+"/puntosActuales").observeSingleEvent(of: .value) { (snapshot) in
                         let points = snapshot.value as? Int ?? 0
                         
                    print (points)
                    self.PointsButton.setTitle( "\(points)" + " puntos", for: .normal)
                           }
                              super.viewDidLoad()
                       }
        // Do any additional setup after loading the view.
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
