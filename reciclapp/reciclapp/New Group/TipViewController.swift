//
//  TipViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/8/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class TipViewController: UIViewController {

    @IBOutlet weak var dailyTip: UILabel!
    var refDB = DatabaseReference.init()
    var arrData = [Tips]()
    func verificar () {
          DispatchQueue.global().async {
                  if Connection.isConnected(vc: self) == false{
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
                                      
                                      alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                      self.present(alert, animated: true)
                                                }
                  
                }
                                
                              }
          
      }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refDB = Database.database().reference()
        
        //Navigation Controller is bigger and has blue color
//        self.navigationController?.navigationBar.prefersLargeTitles = true
        // Do any additional setup after loading the view.
        //refDB.observe(.value, with: { snapshot in
          //print(snapshot.value as Any)
        //})
        changeLabel()
    }
    
    func changeLabel(){
        verificar() 
        refDB.child("recomendaciones/r3/recomendacion").observeSingleEvent(of: .value) { (snapshot) in
            let tip = snapshot.value as? String
            self.dailyTip.text = tip
        }
    }
    
    
}
