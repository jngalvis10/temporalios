//
//  AppDelegate.swift
//  reciclapp
//
//  Created by Paola Latino on 10/3/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import Firebase
import GoogleMaps
import GooglePlaces
//import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    
    //var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        // Database can be available offline as soon as the user connects send the info to th DB
        Database.database().isPersistenceEnabled = true
        //Change Navigation Bar Color
        // navigationBarAppearace.barTintColor = UIColor(hex: "#037Ef3")
        //API Keys
        GMSServices.provideAPIKey("AIzaSyATJUcmWfi1oX3muNQUFQGR2HUNG-nYI2Y")
        GMSPlacesClient.provideAPIKey("AIzaSyATJUcmWfi1oX3muNQUFQGR2HUNG-nYI2Y")
        //self.window = UIWindow(frame: UIScreen.main.bounds)
        //let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //let initialViewController = storyboard.instantiateViewController(withIdentifier: "tabbar") as! UITabBarController
        //self.window?.rootViewController = initialViewController
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    // [START openurl]
//    func application(_ application: UIApplication,
//                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//      return GIDSignIn.sharedInstance().handle(url)
//        return
//    }


}
//converts Hex to rgba for the navigation controller
//extension UIColor {
//    public convenience init?(hex: String) {
//        let r, g, b, a: CGFloat
//
//        if hex.hasPrefix("#") {
//            let start = hex.index(hex.startIndex, offsetBy: 1)
//            let hexColor = String(hex[start...])
//
//            if hexColor.count == 8 {
//                let scanner = Scanner(string: hexColor)
//                var hexNumber: UInt64 = 0
//
//                if scanner.scanHexInt64(&hexNumber) {
//                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
//                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
//                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
//                    a = CGFloat(hexNumber & 0x000000ff) / 255
//
//                    self.init(red: r, green: g, blue: b, alpha: a)
//                    return
//                }
//            }
//        }
//
//        return nil
//    }
//}
//
