//
//  TableViewController.swift
//  UITableViewTemplate
//
//  Created by Paola Latino on 11/24/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import Foundation

struct Headline {

     var id : Int
     var date : Date
     var title : String
     var text : String
     var image : String
    var puntos : String
}

struct MonthSection {
    var month: Date
    var headlines: [Headline]
}

func convertStringToDate(inputDate: String) -> Date {
    //Variable that will help to convert the string to date
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = "yyyy-MM-dd"
    return dateFormat.date(from: inputDate)!
}
class CouponsTableViewController: UITableViewController {

    var sections = [MonthSection]()
    
    // Array with all the data
     var headlines = [
        Headline(id: 1, date: convertStringToDate(inputDate: "2020-05-15"), title: "Palmira Mio", text: "Paga 1 y lleva dos pollos asados en Palmira Mio. Aplican términos y condiciones. No válido para domicilios", image: "Companies-3", puntos: "35"),
        Headline(id: 2, date: convertStringToDate(inputDate: "2020-05-15"), title: "Palmira Mio", text: "40% de descuento en tu siguiente compra de papas a la francesa", image: "Companies-3", puntos: "45"),
        Headline(id: 3, date: convertStringToDate(inputDate: "2020-02-15"), title: "Palmira Mio", text: "8 presas + 1 porción  por solo 28000 pesos. Aplica sólo para domicilios", image: "Companies-3", puntos: "20"),
        Headline(id: 4, date:convertStringToDate(inputDate: "2020-03-05"), title: "Pescadito", text: "Paga un rollo de sushi y recibe ensalada cesar gratis Mio. Aplican términos y condiciones. No válido para domicilios.", image: "Companies-2", puntos: "50"),
        Headline(id: 5, date: convertStringToDate(inputDate: "2020-02-10"), title: "Pescadito", text: "Bono para participar en all you can eat de sushi. Aplica solo para comer en restaurante de 7 a 9 pm.", image: "Companies-1", puntos: "80"),
        Headline(id: 6, date: convertStringToDate(inputDate: "2020-02-10"), title: "Tucón", text: "Válido por un árbol sembrado en el amazonas.", image: "Companies-1", puntos: "10"),
        Headline(id: 7, date: convertStringToDate(inputDate: "2020-02-10"), title: "Tucón", text: "Válido por 4 árboles que se sembrarán en el amazonas.", image: "Companies-1", puntos: "15"),
        Headline(id: 8, date: convertStringToDate(inputDate: "2020-02-10"), title: "Tucón", text: "Válido por 10 árboles que se sembrarán en el amazonas.", image: "Companies-1", puntos: "25"),
       ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Computes the values grouped by date using Dictionary(grouping:by:). Map the result to an Array of MonthSections
        let groups = Dictionary(grouping: headlines) { headline in
            firstDayOfMonth(date: headline.date)
        }
        self.sections = groups.map(MonthSection.init(month:headlines:))
        self.sections.sort { (lhs, rhs) in lhs.month < rhs.month }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
       return self.sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = self.sections[section]
        return section.headlines.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        // Sets the values for the labels and the header
        let section = self.sections[indexPath.section]
        let headline = section.headlines[indexPath.row]
        var allTextCoupon = headline.text+" Puntos:"+headline.puntos
        cell.textLabel?.text = headline.title
        cell.detailTextLabel?.text = allTextCoupon
        cell.imageView?.image = UIImage(named: headline.image)
        
        return cell
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let section = self.sections[section]
        let date = section.month
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        return dateFormatter.string(from: date)
    }
    
    //Computes the first day of the month for a given date
    private func firstDayOfMonth(date: Date) -> Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month], from: date)
        return calendar.date(from: components)!
    }
}
