//
//  Connection.swift
//  reciclapp
//
//  Created by Juan Nicolas Galvis Ortiz on 10/21/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import Foundation
import Network
import SystemConfiguration
import UIKit

class Connection {
  
    
    static func isConnected(vc: UIViewController) -> Bool {
        let monitor = NWPathMonitor()
        var con = true
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                
                print("We're connected!")
                con = true
                
            } else {
                con = false
                 DispatchQueue.main.async {
                                     let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
                                                   
                                                   alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                                   vc.present(alert, animated: true)
                                                             }
                
               
            }
            con = path.isExpensive
             print(path.isExpensive)
        

           
        }
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
        print (con)
            return con
    }
   /* class func Connection() -> Bool{
         
         var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
         zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
         zeroAddress.sin_family = sa_family_t(AF_INET)
         
         let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
             $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                 SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
             }
         }
         
         var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
         if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
             return false
         }
         
         // Working for Cellular and WIFI
         let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
         let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
         let ret = (isReachable && !needsConnection)
         
         return ret
     }*/
    
    
}
