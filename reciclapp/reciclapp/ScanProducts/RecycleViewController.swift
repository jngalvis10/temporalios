//
//  CameraViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/6/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import FirebaseDatabase


class RecycleViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
   var scanner: Scanner?
    var refDB = DatabaseReference.init()
    var product: String = "Error"
    var contador = 0
         var recomendation: String = "Producto no encontrado"
    func verificar () {
          DispatchQueue.global().async {
                  if Connection.isConnected(vc: self) == false{
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
                                      
                                      alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                      self.present(alert, animated: true)
                                                }
                  
                }
                                
                              }
          
      }

    override func viewDidLoad() {
        verificar()
        if Connection.isConnected(vc: self) == true{

                super.viewDidLoad()
                    self.scanner = Scanner(withViewController: self, view: self.view, codeOutputHandler: self.handleCode)
                    self.scanner?.requestCaptureSessionStartRunning()
                     self.refDB = Database.database().reference()
                            
                            //Navigation Controller is bigger and has blue color
                    //        self.navigationController?.navigationBar.prefersLargeTitles = true
                            // Do any additional setup after loading the view.
                            /*refDB.observe(.value, with: { snapshot in
                              print(snapshot.value as Any)
                            })*/
                    
                    
                   print ("ok")

              }
              else {
                         let alert = UIAlertController(title: "Error", message: "Verifique su conexion a internet", preferredStyle: .alert)
                         
                         alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                         self.present(alert, animated: true)
                     }
      
        // Do any additional setup after loading the view.
    }
    
    func handleCode(code: String){
        print (code)
        self.scanner = Scanner(withViewController: self, view: self.view, codeOutputHandler: self.handleCode)
                var user = UserDefaults.standard.string(forKey: "user") ?? "angel"
     
        refDB.child("productos/"+code+"/nombre").observeSingleEvent(of: .value) { (snapshot) in
          let temp = snapshot.value as? String
                    if temp != nil {

                        self.refDB.child("usuariosios/"+user+"/puntosActuales").observeSingleEvent(of: .value) { (snapshot) in
                              var points = snapshot.value as? Int
                            let a = snapshot.value as? String
                            points = points! + 1
                           
                            let postRef = self.refDB.child("usuariosios").child(user).child("puntosActuales")                          
                            postRef.setValue(points)
                         
                                    
                            }
                        self.contador = self.contador + 1
                        print (temp)
                        var cc = "\(self.contador)"
                       //let name = UserDefaults.standard.string(forKey: "email") ?? "Usuario"
                        var name = UserDefaults.standard.integer(forKey: "puntos") ?? 0
                        
                        name = name + self.contador
                        UserDefaults.standard.set(name, forKey: "puntos")
                        let alert = UIAlertController(title: "Producto reciclado", message: "Has acumulado "+cc+" puntos" , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                               self.present(alert, animated: true)
                        
            }
                    else {
                      
                        
                       let alert = UIAlertController(title: "Error", message: "Producto no econtrado", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        self.present(alert, animated: true)
                        
                        
            }
                
        }
       
        
        if let scanner = self.scanner{
            scanner.requestCaptureSessionStartRunning()
            print("inicio camara")
        }

    }
    
    public func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection){
        
        self.scanner?.scannerDelegate(output, didOutput: metadataObjects, from: connection)
        
    }
    
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    /*var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!

    override func viewDidLoad() {
        super.viewDidLoad()

        //view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        print ("created capturesession")

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)

        captureSession.startRunning()
    }

    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }

        dismiss(animated: true)
    }

    func found(code: String) {
        print(code)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }*/
    
}
