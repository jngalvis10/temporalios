//
//  LoginViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/4/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import Firebase
//import GoogleSignIn

class LoginViewController: UIViewController {

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var gmailBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
       var refDB = DatabaseReference.init()
    
    
    override func viewDidLoad() {
        let timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(fire), userInfo: nil, repeats: true)
                super.viewDidLoad()
                self.hideKeyboardWhenTappedAround()
           self.refDB = Database.database().reference()
        //        Login Firebase
                // Do any additional setup after loading the view.
                //gmailBtn.sharedInstance().presentingViewController = self
                
                setupElements()
    }
    func setupElements(){
        // Dissapear error label
        errorLabel.alpha=0
    }
    func verificar () {
        /*if Connection.isConnected() == true{

             print ("ok")

        }
        else {
                   let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
                   
                   alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                   self.present(alert, animated: true)
               }*/
    }
    @objc func fire()
    {
     DispatchQueue.global().async {
              self.verificar()
        if Connection.isConnected(vc: self) == false{
                  DispatchQueue.main.async {
                      let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                    self.present(alert, animated: true)
                                              }
                
              }
                              
                            }
        
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        //Signs in the user in Firebase
        verificar()
        if Connection.isConnected(vc: self) == true {
            
            refDB.child("usuariosios/"+emailTextField.text!+"/email").observeSingleEvent(of: .value) { (snapshot) in
                  let email = snapshot.value as? String
                            if email != nil {
                                
                              guard
                                         
                                let password = self.passwordTextField.text,
                                          
                                email!.count > 0,
                                          password.count > 0
                                          else {
                                            return
                                        }
                                        UserDefaults.standard.set(email, forKey: "email")
                                        UserDefaults.standard.set(self.emailTextField.text!, forKey: "user")
                          
                                        UserDefaults.standard.set(0, forKey: "puntos")
                                       
                                        if Connection.isConnected(vc: self) == true{

                                            Auth.auth().signIn(withEmail: email!, password: password) { user, error in
                                          if let error = error, user == nil {
                                            let alert = UIAlertController(title: "El ingreso falló", message: "",
                                                                         
                                                                          preferredStyle: .alert)
                                            self.showError(mensaje: "El ingreso falló")
                                            alert.addAction(UIAlertAction(title: "OK", style: .default))
                                            self.present(alert, animated: true, completion: nil)
                                          }
                                          else{
                                             self.goToHome()
                                            }
                                        }
                                            
                                        print("hizo login")
                                        }
                                        else {
                                                   let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
                                                   
                                                   alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                                   self.present(alert, animated: true)
                                               }
                                
                    }
                            else {
                                
                                let alert = UIAlertController(title: "Error", message: "Usuario no encontrado", preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                self.present(alert, animated: true)
                                
                    }
                        
                }
            
          
            //Sends to the Home Screen
        }
        else{
            verificar()
        }
        
       
    }
    
    func showError(mensaje : String){
        errorLabel.text = mensaje
        errorLabel.alpha = 1
    }
//     // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
//         self.transicionAlHome()
    }
    
   func goToHome(){
    if Connection.isConnected(vc: self) == true {
        print ("Rumbo a home")
             let homeViewController = storyboard?.instantiateViewController (withIdentifier: "tabbar") as! UITabBarController
        
         
             view.window?.rootViewController = homeViewController
             view.window?.makeKeyAndVisible()
         print ("Lo logramos")
    }
    else {
        verificar()
    }
   
    
    
    }

 
    @IBAction func newTapped(_ sender: Any) {
        verificar()
        let homeViewController = storyboard?.instantiateViewController (withIdentifier: "RegistrationVC") as! UIViewController
        
         
             view.window?.rootViewController = homeViewController
             view.window?.makeKeyAndVisible()
         print ("Lo logramos")
        
    }
    
}
