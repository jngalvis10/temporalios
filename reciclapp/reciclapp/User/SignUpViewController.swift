//
//  SignUpViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/4/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
//import GoogleSignIn
import FirebaseDatabase


class SignUpViewController: UIViewController {

    // [START Outlets]
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var confirmationPassTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var genderField: UISegmentedControl!
    @IBOutlet weak var birthDateField: UIDatePicker!
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    var cleanDate = ""
    var cleanGender = "Masculino"
    var ref: DatabaseReference!
    
    // [END Outlets]
    
    // [START viewdidload]
    override func viewDidLoad() {
        let timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(fire), userInfo: nil, repeats: true)
                       super.viewDidLoad()
                       self.hideKeyboardWhenTappedAround()
               //        Login Firebase
                       // Do any additional setup after loading the view.
                       //gmailBtn.sharedInstance().presentingViewController = self
                       
                      setUpElements()

        }
    
    
    @IBAction func backTapped(_ sender: Any) {
        verificar()
        let homeViewController = storyboard?.instantiateViewController (withIdentifier: "LoginVC") as! UIViewController
               
                
                    view.window?.rootViewController = homeViewController
                    view.window?.makeKeyAndVisible()
                print ("lo logramos")
    }
    func verificar () {
       /* if Connection.isConnected() == true{

             print ("ok")

        }
        else {
                   let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
                   
                   alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                   self.present(alert, animated: true)
               }*/
    }
    
    @objc func fire()
    {
        DispatchQueue.global().async {
              self.verificar()
              if Connection.isConnected(vc: self) == false{
                  DispatchQueue.main.async {
                      let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                    self.present(alert, animated: true)
                                              }
                
              }
                              
                            }
        }
    // check the gender selected
    @IBAction func cambioGenero(_ sender: Any) {
        switch genderField.selectedSegmentIndex
        {
        case 0:
            cleanGender = "Masculino"
        case 1:
            cleanGender = "Femenino"
        case 2:
            cleanGender = "Otro"
        default:
            break
        }
    }
    
    @IBAction func cambioFecha(_ sender: Any) {
        verificar()
        var timeFormatter = DateFormatter()
        timeFormatter.dateStyle = DateFormatter.Style.short
        cleanDate = timeFormatter.string(from: birthDateField.date)
       
        
    }
    
    //Check the fields and validates if the input data is correct. In case something is wrong, returns error message else returns nil.
    func validateFields() -> String?{
        //Checks that all fields are filled
        if nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || confirmationPassTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Por favor llene todos los campos."
        }
        
        else {
        //Validates Password
        let cleanPassword = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let cleanConfirmation = confirmationPassTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
             let isEqual = (cleanPassword == cleanConfirmation)
                       if Utilities.isPasswordValid(cleanPassword!) == false {
                           return "La contraseña debe tener mínimo 8 caracteres, un número y un caracter especial (Ej: $%&#@!)."
                       }
                       else if isEqual == false {
                            return "Las contraseñas no coinciden "
                       }
                    
        }
        return nil
    }
    
    func setUpElements(){
        // Dissapear error label
        errorLabel.alpha = 0
        //Give style to the text fields in the view
        //
        
    }
        
       
    @IBAction func signUpTapped(_ sender: Any) {
      verificar()
        switch Connection.isConnected(vc: self)
        {
        case true:
               
                     if Connection.isConnected(vc: self) == false{
                         showErrorMessage(message:  "No hay conexión")
                     }
                     else {
                             let error = validateFields()
                             if error != nil{
                                 //Error on any of the fields
                                 showErrorMessage(message: error!)
                             }
                             else{
                               //Create the user
                                 Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { user, error in
                                          if error == nil {
                                            
                                     
                                            UserDefaults.standard.set(self.emailTextField.text!, forKey: "email")
                                            UserDefaults.standard.set(self.userTextField.text!, forKey: "user")
                                                          UserDefaults.standard.set(0, forKey: "puntos")
                                            
                                           
                                            // let emailText = self.emailTextField.text
                                             //ref.child("usuarios/" + self.emailTextField.text!).setValue(["email": emailText, "nombre": self.nameTextField.text,"genero":self.cleanGender, "nacimiento":self.cleanDate, "puntosActuales": "0", "puntosAcumulados" :"0" ])
                     //                        ref.childByAutoId().setValue(["email": emailTextField.text , "nombre": nameTextField.text,"genero":cleanGender, "nacimiento":cleanDate, "puntosActuales": "0", "puntosAcumulados" :"0" ])
                                          }
                                        }
                               //Go to Home Screen
                                 self.ref = Database.database().reference()
                                let postRef = self.ref.child("usuariosios").child(userTextField.text!)
                                                   let postObject = ["user": userTextField.text!, "email": emailTextField.text!, "nombre": self.nameTextField.text,"genero":self.cleanGender, "nacimiento":self.cleanDate, "puntosActuales": 0, "puntosAcumulados" :0 ] as [String : Any]
                                                   postRef.setValue(postObject)
                                 self.goToHome()
                             }
                     }
        case false:
             let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
                       
                       alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                       self.present(alert, animated: true)
                       showErrorMessage(message: "No hay conexión a internet")
            break
       
      
        }
        
       
       /* if Connection.isConnected() == true{
              //Validate Fields
            
            if Connection.isConnected() == false{
                showErrorMessage(message:  "no hay conexión")
            }
            else {
                    let error = validateFields()
                    if error != nil{
                        //Error on any of the fields
                        showErrorMessage(message: error!)
                    }
                    else{
                      //Create the user
                        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { user, error in
                                 if error == nil {
                            
                                    
                                  
                                   // let emailText = self.emailTextField.text
                                    //ref.child("usuarios/" + self.emailTextField.text!).setValue(["email": emailText, "nombre": self.nameTextField.text,"genero":self.cleanGender, "nacimiento":self.cleanDate, "puntosActuales": "0", "puntosAcumulados" :"0" ])
            //                        ref.childByAutoId().setValue(["email": emailTextField.text , "nombre": nameTextField.text,"genero":cleanGender, "nacimiento":cleanDate, "puntosActuales": "0", "puntosAcumulados" :"0" ])
                                 }
                               }
                      //Go to Home Screen
                        self.ref = Database.database().reference()
                                   let postRef = self.ref.child("usuarios").childByAutoId()
                                          let postObject = ["email": emailTextField.text!, "nombre": self.nameTextField.text,"genero":self.cleanGender, "nacimiento":self.cleanDate, "puntosActuales": "0", "puntosAcumulados" :"0" ] as [String : Any]
                                          postRef.setValue(postObject)
                        self.goToHome()
                    }
            }
        }
        else {
           let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
            showErrorMessage(message: "No hay conexion a internet")
            
        }
      */
        
        
    }
    
    //Makes error label visible and assignes the error
    func showErrorMessage( message:String){
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    func goToHome(){
        verificar()
        if Connection.isConnected(vc: self) == true{
       print (" rumbo a home")
            let homeViewController = storyboard?.instantiateViewController (withIdentifier: "tabbar") as! UITabBarController
        print ("creado el let")
        
            view.window?.rootViewController = homeViewController
            view.window?.makeKeyAndVisible()
        print ("lo logramos")
        }
    
    }
    
  
    
}

     
